require 'test_helper'

class UfeaturesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get ufeatures_index_url
    assert_response :success
  end

  test "should get new" do
    get ufeatures_new_url
    assert_response :success
  end

  test "should get create" do
    get ufeatures_create_url
    assert_response :success
  end

  test "should get show" do
    get ufeatures_show_url
    assert_response :success
  end

  test "should get edit" do
    get ufeatures_edit_url
    assert_response :success
  end

  test "should get update" do
    get ufeatures_update_url
    assert_response :success
  end

  test "should get destroy" do
    get ufeatures_destroy_url
    assert_response :success
  end

end
