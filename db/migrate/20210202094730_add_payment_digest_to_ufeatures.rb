class AddPaymentDigestToUfeatures < ActiveRecord::Migration[5.2]
  def change
    add_column :ufeatures, :validity, :string
  end
end
