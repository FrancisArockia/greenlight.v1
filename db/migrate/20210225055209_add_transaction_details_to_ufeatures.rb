class AddTransactionDetailsToUfeatures < ActiveRecord::Migration[5.2]
  def change
    add_column :ufeatures, :mid, :string
    add_column :ufeatures, :pid, :string
    add_column :ufeatures, :xid, :string
    add_column :ufeatures, :trans_id, :string
    add_column :ufeatures, :trans_type, :string
    add_column :ufeatures, :pncr, :string
    add_column :ufeatures, :cc_expiry, :string
    add_column :ufeatures, :cc_brand, :string
    add_column :ufeatures, :code, :string
    add_column :ufeatures, :status, :string
    add_column :ufeatures, :description, :string
  end
end
