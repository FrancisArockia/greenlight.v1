class AddPaymentToUfeatures < ActiveRecord::Migration[5.2]
  def change
    add_column :ufeatures, :pay_type, :string
    add_column :ufeatures, :pay_amount, :decimal
    add_column :ufeatures, :pay_status, :string
  end
end
