class AddUfeatureSettingsToUfeature < ActiveRecord::Migration[5.2]
  def change
    add_column :ufeatures, :ufeature_settings, :string, default: "{ }"
  end
end
