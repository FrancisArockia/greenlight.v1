class CreateUfeatures < ActiveRecord::Migration[5.2]
  def change
    create_table :ufeatures do |t|
      t.belongs_to :user, index: true
      t.string "name", null: false
      t.string "value"
      t.boolean "enabled", default: false
      t.datetime :start_date, null: false
      t.datetime :expiry_date, null: false
      t.string :remaining_days, null: false

      t.timestamps
    end
  end
end
