class Ufeature < ApplicationRecord

    include Deleteable

    # before_create :setup
    # before_create :setupFor
  
    before_destroy :destroy_presentation
  
    validates :name, presence: true
  
    belongs_to :owner, class_name: 'User', foreign_key: :user_id
    has_many :shared_access
  
    has_one_attached :presentation

    # Determines if a user owns a room.
  def owned_by?(user)
    user_id == user&.id
  end

  def shared_users
    User.where(id: shared_access.pluck(:user_id))
  end

  def shared_with?(user)
    return false if user.nil?
    shared_users.include?(user)
  end

  private

  # Generates a uid for the room and BigBlueButton.
  def to_param(subscribe_params)
    # logger.info 'uuuuuuuuuuuuuuuuuuuuuuuuuuuuu'
    # print log in params value ufeature_params
    # logger.info "U-Features params: #{subscribe_params}"
  end

  def setup
    
    redirect_to params[:ufeature] || create_user_path

    # logger.info 'ppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp'
    # print log in params value ufeature_params
    # logger.info "U-Features params: #{params[:pay_type]}"

    self.value = '3'
    self.enabled = false
    self.pay_type = 'Free'
    self.pay_status = 'Success'
    self.validity = '7 Days only'

    # self.value = '5'
    # self.enabled = false
    # self.pay_type = 'Basic'
    # self.pay_status = 'Success'
    # self.validity = 'OneMonth'

    # self.value = '10'
    # self.enabled = false
    # self.pay_type = 'Premium'
    # self.pay_status = 'Success'
    # self.validity = 'OneYear'
  end

   # Before destroying the , make sure you also destroy the presentation attached
    def destroy_presentation
    presentation.purge if presentation.attached?
  end
end
