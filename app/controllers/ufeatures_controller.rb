class UfeaturesController < ApplicationController
  def index
  end

  def new
  end

  def create

    # @ufeature = Ufeature.new(name: ufeature_params[:name])
    @ufeature = Ufeature.new(ufeature_params)

    # logger.info 'ppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp'
    # print log in params value ufeature_params
    # logger.info "U-Features params: #{ufeature_params}"
    # logger.info 'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
    # logger.info "params : #{params}"

    @ufeature.owner = current_user
    @ufeature.ufeature_settings = create_ufeature_settings_string(ufeature_params)
  end

  def create_ufeature_settings_string(options)
    ufeature_settings = {
      "muteOnStart": options[:mute_on_join] == "1",
      "requireModeratorApproval": options[:require_moderator_approval] == "1",
      "anyoneCanStart": options[:anyone_can_start] == "1",
      "joinModerator": options[:all_join_moderator] == "1",
      "recording": options[:recording] == "1",
    }

    ufeature_settings.to_json
  end

  def show
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private

  def ufeature_params
    params.require(:ufeature).permit(:name, :value, :enabled, :pay_type, :pay_status, :validity)
  end

end
